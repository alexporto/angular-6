## Angular 6

#### Seção: 1 - Getting Started

[6 - Editing the First App](/session/01_06)

[10 - A Basic Project Setup using Bootstrap for Styling](/session/01_10)

11 - Where to find the Course Source Code

 - [Fist](/session/01_11_fist)

 - [Template](/session/01_11_template)


#### Seção: 2

[21 - Fully Understanding the Component Selector](/session/02_21)

##### [Tarefa 01](/task/01)

[31. Combining all Forms of Databinding](/session/02_31)

##### [Tarefa 02](/task/02)

##### [Tarefa 03 - Practicing Directives](/task/03)

[38. Getting the Index when using ngFor](/session/02_31)

 - [Components](/session/02_38_components)
 - [Databinding](/session/02_38_databinding)
 - [Final](/session/02_38_final)
 - [ngFor](/session/02_38_ngfor)


#### Seção: 3
#### Seção: 4
#### Seção: 5

##### [Tarefa 04](/task/04)

#### Seção: 4
#### Seção: 6
[Course Project - Components & Databinding](/session/06)
#### Seção: 7
[Debugging](/session/04)

#### Seção: 6 
[Seção: 6 | Course Project - Components & Databinding](/session/066)

[Seção: 7 | Directives Deep Dive](/session/07)

