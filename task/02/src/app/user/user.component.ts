import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
    userCreationStatus = 'No user was created!';
    userName = '';

  constructor() { }

  ngOnInit() {

  }

  onCreaterName() {
      this.userCreationStatus = 'User was created! Name is ' + this.userName;
  }

    onClearUserName() {
      this.userName = '';
      this.userCreationStatus = 'User was cleaned!';
  }

}
